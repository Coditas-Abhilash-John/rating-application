import { Component, OnInit } from '@angular/core';
import { IRating } from 'src/app/app.types';

@Component({
  selector: 'app-rating-page',
  templateUrl: './rating-page.component.html',
  styleUrls: ['./rating-page.component.css']
})
export class RatingPageComponent implements OnInit {

  ratingData :IRating[] = []
  constructor() { }

  ngOnInit(): void {
  }

  onRatingSubmit(rating:IRating){
    rating.average = Math.abs((rating.ambience+rating.cleanliness+rating.food+rating.service)/4)
    this.ratingData.push(rating);
  }
}
