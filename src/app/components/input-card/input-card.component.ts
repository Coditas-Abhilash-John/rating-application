import { Component, Input, OnInit} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-card',
  templateUrl: './input-card.component.html',
  styleUrls: ['./input-card.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputCardComponent,
      multi: true,
    },
  ],
})
export class InputCardComponent implements OnInit, ControlValueAccessor{
  onChange!: (value: string) => void;
  @Input() label =""
  inputValue :string = "";
  writeValue(obj: any): void {
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
  }

  ngOnInit(): void {

  }

}
