import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  constructor() { }
  @Input() header = ""
  @Input() ratingValue = 0;
  ngOnInit(): void {
  }

  ratingColorScale : string[]= ['#ff4545','#ff4500','#ffa534','#ffa500','#ffe234','#ffe234','#b7dd29','#b7dd29','#57e32c','#47e32c','#17e32c']

  getStyle(){
    if(!this.ratingValue){
      this.ratingValue=0
    }
    return{
      'width' : `${this.ratingValue*10}%`,
      'background-color': this.ratingColorScale[this.ratingValue]
    }
  }
}
