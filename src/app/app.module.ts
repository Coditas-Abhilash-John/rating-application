import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RatingPageComponent } from './pages/rating-page/rating-page.component';
import { RatingComponent } from './features/rating/rating.component';
import { DisplayRatingComponent } from './features/display-rating/display-rating.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputCardComponent } from './components/input-card/input-card.component';
import { SliderComponent } from './components/slider/slider.component';

@NgModule({
  declarations: [
    AppComponent,
    RatingPageComponent,
    RatingComponent,
    DisplayRatingComponent,
    InputCardComponent,
    SliderComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
