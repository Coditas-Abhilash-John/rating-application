import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IRating } from 'src/app/app.types';

@Component({
  selector: 'app-display-rating',
  templateUrl: './display-rating.component.html',
  styleUrls: ['./display-rating.component.css']
})
export class DisplayRatingComponent implements OnInit{

  constructor() { }


  @Input() ratingInfo : IRating[] = []

  ngOnInit(): void {
  }

  getAverage(key: "ambience" | "cleanliness" | "service" | "food" | "average" ){
    let data = Math.floor(this.ratingInfo.reduce((sum,rating)=>sum + rating[key],0)/this.ratingInfo.length)
    return data

  }
}
