import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IRating } from 'src/app/app.types';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  @Output() submitRating = new EventEmitter<IRating>();
  hotelRating = new FormGroup({
    ambience : new FormControl("",[Validators.min(0), Validators.max(10), Validators.required]),
    cleanliness : new FormControl("",[Validators.min(0), Validators.max(10), Validators.required]),
    service : new FormControl("",[Validators.min(0), Validators.max(10), Validators.required]),
    food : new FormControl("",[Validators.min(0), Validators.max(10), Validators.required]),
  })

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(formGroupInput:FormGroup){
    if(!formGroupInput.invalid){
      this.submitRating.emit(formGroupInput.value);
    }
  }
}
