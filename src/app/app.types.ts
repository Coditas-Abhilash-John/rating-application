export interface IRating{
  ambience:number,
  cleanliness:number,
  service: number,
  food:number,
  average : number
}
